import React from "react";

class Profile extends React.Component {
  render() {
    return (
      <div>
        <h3>component profile </h3>
        {this.props.contact()}
      </div>
    );
  }
}

export default Profile;
