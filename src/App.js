import "./App.css";
import Firstname from "./components/firstName";
import LastName from "./components/LastName";
import Profile from "./components/profileClass";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Button from "./components/Button";

const App = () => {
  const klik = () => {
    return alert(`Halo Semua !!`);
  };

  const contact = () => {
    return <i>ini contact</i>;
  };
  return (
    <div className="App">
      <Navbar />
      <Profile contact={contact} />
      <h1>Hello World ✋!</h1>
      <Firstname name="Uttungga" />
      <LastName name="Hary" />
      <Footer />
      <Button BtnClick={klik} />
    </div>
  );
};

export default App;
